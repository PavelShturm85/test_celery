from celery import Celery
from celery.schedules import crontab
from .settings import PATHs, INCLUDE_LIST, BROKER, ROUTES
from .settings_schedule import CELERYBEAT

app = Celery('celery_queues',
             broker=BROKER,
             backend=BROKER,
             include=INCLUDE_LIST
            )

# Optional configuration, see the application user guide.
app.conf.update(
    result_expires=3600,
    
)
app.conf.task_routes = ROUTES
app.conf.beat_schedule = CELERYBEAT

app.conf.timezone = 'UTC'


if __name__ == '__main__':
    # start in proj dir.
    # celery -A celery_queues worker -l info -n worker_{QUEUE}@%h -Q {QUEUE}
    # -Q social_web
    # -Q reports
    # -Q news
    # celery -A celery_queues beat -l info
    app.start()