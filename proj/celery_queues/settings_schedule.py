from .settings import PATHs

CELERYBEAT = {
    'check_news_one-every-10-seconds': {
        'task': f'{PATHs["news"]}.check_news_one',
        'schedule': 10.0,
        'args': None,
    },
    'check_news_two-every-3-seconds': {
        'task': f'{PATHs["news"]}.check_news_two',
        'schedule': 7.0,
        'args': None,
    },
    'check_news_three-every-15-seconds': {
        'task': f'{PATHs["news"]}.check_news_three',
        'schedule': 15.0,
        'args': None,
    },
}