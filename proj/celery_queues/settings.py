PATHs = dict(
    news="celery_queues.news.tasks",
    reports="celery_queues.reports.tasks",
    social_web="celery_queues.social_web.tasks"
)

INCLUDE_LIST = [PATHs[key] for key in PATHs]

ROUTES = {f'{PATHs[key]}.*': {'queue': key} for key in PATHs}

BROKER = 'amqp://'